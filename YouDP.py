import msvcrt as m
import pickle
import socket
from threading import Thread
from time import sleep

import fire

import log_helper
from udp_message import UDPMessage

messages = list()
buffer_size = 16384
still_listening = True
messages_filename = 'messages.pickle'

log = log_helper.get_logger("YouDP")


def stop_listening():
    global still_listening
    still_listening = False
    with open(messages_filename, 'wb') as output_file:
        pickle.dump(messages, output_file)
    log.info('Saving collected messages into {}'.format(messages_filename))


def listen(port: int):
    global still_listening
    udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    udp_socket.settimeout(1)
    udp_socket.bind(('0.0.0.0', port))
    log.debug('Listening for UDP on port {}'.format(port))
    while still_listening:
        try:
            byte_array, address = udp_socket.recvfrom(buffer_size)
            messages.append(UDPMessage(byte_array=byte_array))
            log.debug('Received {} bytes from {} ({} total collected messages)'.format(len(byte_array), address, len(messages)))
        except:
            pass


def record(port: int, filename: str = None):
    global messages_filename
    if filename is not None:
        messages_filename = filename
    thread = Thread(target=listen, args=(port,))
    thread.start()
    m.getch()
    stop_listening()


def play(target_ip: str, port: int, filename: str = None):
    global messages
    global messages_filename
    if filename is not None:
        messages_filename = filename
    log.debug('Reading messages from {}'.format(messages_filename))
    with open(messages_filename, 'rb') as input_file:
        messages = pickle.load(input_file)
        udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        for index, message in enumerate(messages):
            log.debug('Transmitting message number {} ({} bytes) to {}:{}'.format(index + 1, len(message.byte_array), target_ip, port))
            udp_socket.sendto(message.byte_array, (target_ip, port))
            if len(messages) > index + 1:
                seconds_to_sleep = messages[index + 1].timestamp - message.timestamp
                log.debug('Pause for {:.2f} seconds'.format(seconds_to_sleep))
                sleep(seconds_to_sleep)
        log.info('Done playing, goodbye.')


if __name__ == '__main__':
    fire.Fire()
