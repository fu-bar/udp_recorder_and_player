import time


class UDPMessage:
    def __init__(self, byte_array: bytes):
        self.timestamp = time.time()
        self.byte_array = byte_array
