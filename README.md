# UDP Recorder and Player



## Examples of Execution

**Record**  
python YouDP.py record --port 9090  
_(press any key to stop recording)_    

**Replay**  
python YouDP.py play --target_ip 127.0.0.1 --port 54839

```
C:\workspace\fubar\YouDP>python YouDP.py record --port 9090
[10-10-2021 20:54:39] YouDP DEBUG: Listening for UDP on port 9090
[10-10-2021 20:54:47] YouDP DEBUG: Received 5 bytes from ('127.0.0.1', 50882) (1 total collected messages)
[10-10-2021 20:54:52] YouDP DEBUG: Received 5 bytes from ('127.0.0.1', 50882) (2 total collected messages)
[10-10-2021 20:54:53] YouDP DEBUG: Received 5 bytes from ('127.0.0.1', 50882) (3 total collected messages)
[10-10-2021 20:54:57] YouDP INFO: Saving collected messages into messages.pickle

C:\workspace\fubar\YouDP>python YouDP.py play --target_ip 127.0.0.1 --port 54839
[10-10-2021 20:55:32] YouDP DEBUG: Reading messages from messages.pickle
[10-10-2021 20:55:32] YouDP DEBUG: Transmitting message number 1 (5 bytes) to 127.0.0.1:54839
[10-10-2021 20:55:32] YouDP DEBUG: Pause for 4.66 seconds
[10-10-2021 20:55:36] YouDP DEBUG: Transmitting message number 2 (5 bytes) to 127.0.0.1:54839
[10-10-2021 20:55:36] YouDP DEBUG: Pause for 1.20 seconds
[10-10-2021 20:55:37] YouDP DEBUG: Transmitting message number 3 (5 bytes) to 127.0.0.1:54839
[10-10-2021 20:55:37] YouDP INFO: Done playing, goodbye.
```
