import logging
import multiprocessing
import sys


def get_logger(logger_name: str = multiprocessing.current_process().name) -> logging.Logger:
    date_time_format = "%d-%m-%Y %H:%M:%S"
    log_row_format = "[%(asctime)s] %(name)s %(levelname)s: %(message)s"
    log_row_format = logging.Formatter(fmt=log_row_format, datefmt=date_time_format)

    file_handler = logging.FileHandler(filename='YouDP.log', mode='a', encoding='utf-8')
    file_handler.setFormatter(log_row_format)

    console_handler = logging.StreamHandler(sys.stdout)
    console_handler.setFormatter(log_row_format)

    log = logging.getLogger(logger_name)

    log.addHandler(console_handler)
    log.addHandler(file_handler)

    log.setLevel(logging.DEBUG)

    return log
